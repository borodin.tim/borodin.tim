# Hi, I'm Timur Borodin

I'm a web developer

[![Linkedin](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/timurborodin/) [![GitHub](https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white)](https://github.com/De1ain) 


## Stack

- NodeJS 🚀
- Express 🕑
- React ⚛
- Redux 🔥
- MongoDB 🍃


## Featured Projects
- [Blog built on Gatsby](https://gatsby-blog-timb.netlify.app/blog) | [[Code](https://gitlab.com/borodin.tim/gatsbyblog)]
- [GitProfiles](https://github-gitlab-profiles.netlify.app/) [[Code](https://gitlab.com/borodin.tim/gitprofiles)]
- [Drawing App](https://drawing-app-jscanvas.netlify.app/) [[Code](https://gitlab.com/borodin.tim/drawingapp)]
- [Movie App](https://movies-app-nodejs-dev.herokuapp.com/) [[Code](https://gitlab.com/borodin.tim/movieapp)]
- Israel Trail Tracking | [[Code](https://gitlab.com/borodin.tim/israel-trail)]
